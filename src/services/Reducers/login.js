import { IS_LOGGEDIN } from "../constents";
import {v4} from "uuid"

const initialState = {
    id:v4(),
    email:"",
    password:"",
    username:"",
    isLoggedIn:false
}

const userLoggedIn = (state = initialState,action)=>{

    switch(action.type){
        case IS_LOGGEDIN:
            return {...action.data,isLoggedIn:!state.isLoggedIn}
        default:
            return state
    }
}

export default userLoggedIn;