import { combineReducers } from "redux";
import addUser from "./login";
import articles from "./Articels"

export default combineReducers({
    addUser,
    articles
})