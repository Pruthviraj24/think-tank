
import { GET_ARTICLES, UPDATE_FAV,UPDATE_FOLLOWING,UPDATE_TAG_BASED} from "../constents";

const initialState = {
    articlesData:[],
    tags:[],
    articles:[]
}

const articles = (state=initialState,action) =>{

    switch(action.type){
        case GET_ARTICLES:
            const tagsArray  = action.data.slice(6).reduce((acc,eachBlog) =>{
                 acc = [...acc, ...eachBlog.tagList] 
                 return acc
            },[])

            const updatedWithCommants = action.data.slice(6).map(eachBlog => {
                return{
                    ...eachBlog,
                    comments:[]
                }
            })
            return {
                ...state,
                articlesData:updatedWithCommants,
                tags:tagsArray,
                articles:action.data.slice(6)
            }
        case UPDATE_FAV:
            const updatedData = state.articlesData.map(eachBlog => {
                if(eachBlog.slug === action.data.slug){
                        eachBlog.favorited = !eachBlog.favorited
                        return eachBlog
                }else{
                    return eachBlog
                }
            })
    
            return {
                ...state,
                articlesData:updatedData
            }
        case UPDATE_FOLLOWING:
            const updatedFollowingState = state.articlesData.map(eachBlog => {
                if(eachBlog.author.username === action.data.author.username){
                        eachBlog.author.following = !eachBlog.author.following

                        return eachBlog
                }else{
                    return eachBlog
                }
            })
            
            return {
                ...state,
                articlesData:updatedFollowingState
            }
        case UPDATE_TAG_BASED:
          
            const updatedTagBasedState = state.articles.filter(eachBlog => eachBlog.tagList.includes(action.tag))

            return {
                ...state,
                articlesData:updatedTagBasedState
            }
        default:
            return state
    }
}   

export default articles