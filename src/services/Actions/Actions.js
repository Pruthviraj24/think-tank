import {IS_LOGGEDIN,ADD_USER,GET_ARTICLES,UPDATE_FAV,UPDATE_FOLLOWING,UPDATE_TAG_BASED} from "./../constents"

export const userLoggedIn = (data)=>{
    return{
        type:IS_LOGGEDIN,
        data:data
    }
}

export const addUser = (data)=>{
    return{
        type:ADD_USER,
        data:data
    }
}

export const updateArticles= (data)=>{
    return{
        type:GET_ARTICLES,
        data:data
    }
}

export const updateFav = (data)=>{
    return{
        type:UPDATE_FAV,
        data:data
    }
} 

export const followingState = (data)=>{
    return{
        type:UPDATE_FOLLOWING,
        data:data
    }
} 


export const tagBasedFilter = (tag)=>{
    return{
        type:UPDATE_TAG_BASED,
        tag:tag
    }
}