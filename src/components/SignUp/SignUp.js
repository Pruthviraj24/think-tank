import { Component } from 'react'
import { connect } from 'react-redux'
import Cookies from 'js-cookie'
import userLoggedIn from "../../services/Reducers/login"
import "./index.css"

class SignUp extends Component {
  state = {name:"",email:"",password:""}

  updateName = (event)=>{
    this.setState({
      name:event.target.value
    })
  }

  updateEmail = (event) =>{
    this.setState({
      email:event.target.value
    })
  }
    
  updatePassword = (event)=>{
    this.setState({
      password:event.target.value
    })
  }


  onSubmitFailure = (error)=>{
    // console.log(error);
}

onSubmitSuccess = (jwtToken)=>{
  const {history} = this.props
  Cookies.set('jwt_token', jwtToken, {
    expires: 30,
    path: '/',
  })
  history.replace('/')
}

  submitSignUp = async (event)=>{
    event.preventDefault()
    
    const {name,email,password} = this.state

    const userDetails = {user:{username:name,email:email,password:password}}

    const url = 'https://mighty-oasis-08080.herokuapp.com/api/users/'
    const options = {
      method: 'POST',
      headers:{"Content-Type" : "application/json"},
      body: JSON.stringify(userDetails)
    }
    const response = await fetch(url, options)

    if (response.ok === true) {
      const responseData = await response.json()
      this.onSubmitSuccess(responseData.user.token)
    } else {
      this.onSubmitFailure(response.error_msg)
    }
  }


  render() {
    return (
      <form onSubmit={this.submitSignUp} id="signup">
      <div className='input-parent'>
        <label htmlFor='username'>Username</label>
        <input id="username" onChange={this.updateName} placeholder='Your Name' type="text"/>
      </div>
      <div className='input-parent'>
        <label htmlFor='SignUp-email'>Email</label>
        <input id="SignUp-email" onChange={this.updateEmail} placeholder='Your Email' type="email"/>
      </div>
      <div className='input-parent'>
        <label htmlFor='signup-password'>Password</label>
        <input id='signup-password' onChange={this.updatePassword} placeholder='Your Password' type="password"/><br/>
      </div>
        <button type="submit">Sign Up</button>
      </form>
    )
  }
}

const mapStateToProps = state =>({
  userData : state.userData
})



const mapDispatchToProps = (dispatch)=>{
  return{
    getLoggeddStatus: (data)=>dispatch(userLoggedIn(data)),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(SignUp);