import { Component } from 'react'
import {connect} from "react-redux"
import BlogItem from '../BolgItem/BlogItem'
import {updateArticles,tagBasedFilter} from "./../../services/Actions/Actions.js"

import "./index.css"

class Home extends Component {


    componentDidMount(){
        this.getArticlesData()
    }

    getArticlesData = async()=>{
        const url = "/api/articles"

        const response = await fetch(url)
    
        if(response.ok){
            const responseData = await response.json()
            const {getArticlesHandler} = this.props
            getArticlesHandler(responseData.articles)
        }else{
            console.log("Log error");
        }
    }

    updateFilteredBlogs = (event)=>{
      const {tagBasedFilterHandler} = this.props
      tagBasedFilterHandler(event.target.value)
    }

  render() {
    const {articlesData,tagsData} = this.props
    return (
      <>
      {articlesData.length > 0 ? 
      <div className="blogs-tags-main-container">
      <ul className='blog-list wrapper'>
         {
          articlesData.map(eachBlog => <BlogItem blog={eachBlog} key={eachBlog.slug}/>)
         }
      </ul> 
      <div className='tags-container'>
            {
              // eslint-disable-next-line array-callback-return
              tagsData.map(eachTag => <button value={eachTag} onClick={this.updateFilteredBlogs} className='tag-button' type="button">{eachTag}</button>)
            }
          </div>
        </div>
      : <></>}
      
      </>
    )
  }
}

const mapStateToProps = (state)=>{

  return{
    articlesData:state.articles.articlesData,
    tagsData:state.articles.tags
  }
}


const mapDispatchToProps = (dispatch)=>{
  return{
    getArticlesHandler: (data)=>dispatch(updateArticles(data)),
    tagBasedFilterHandler:(tag)=>dispatch(tagBasedFilter(tag))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Home)