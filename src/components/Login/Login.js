import { Component } from 'react'
import Cookies from 'js-cookie'
import {userLoggedIn} from "./../../services/Actions/Actions"
import {connect} from "react-redux"
import "./index.css"

class Login extends Component {
   state = {
    email:"",
    password:"",
}

    updatePassword = (event)=>{
        this.setState({
            password:event.target.value
        })
    }

    updateEmail = (event)=>{
        this.setState({
            email:event.target.value
        })
    }

    onSubmitFailure = (error)=>{
        // console.log(error);
    }

    onSubmitSuccess = (user)=>{
      const {history,getLoggeddStatus} = this.props
      getLoggeddStatus(user);
      Cookies.set('jwt_token', user.token, {
        expires: 30,
        path: '/',
      })
      history.replace('/')
     
  
    }


    submitForm = async (event)=>{
        event.preventDefault()
        const {email, password} = this.state
       
        const userDetails = {user:{email, password}}
        const url = 'https://mighty-oasis-08080.herokuapp.com/api/users/login/'
    
        const options = {
          method: 'POST',
          headers:{"Content-Type" : "application/json"},
          body: JSON.stringify(userDetails),
        }
        const response = await fetch(url, options)

        if (response.ok === true) {
          const responseData = await response.json()
          this.onSubmitSuccess(responseData.user)
        } else {
          this.onSubmitFailure(response.error_msg)
        }
      }
    


  render() {
    const {email,password} = this.state



    return (

      <form onSubmit={this.submitForm} className="login-form" id="main">
        <div className="input-parent">
            <label htmlFor='mail'>Email</label>
            <input id="mail" value={email} type="email" onChange={this.updateEmail}  />
        </div>
        <div className="input-parent">
            <label htmlFor='pass'>Password</label>
            <input id="pass" password={password} type="password"  onChange={this.updatePassword}/>
        </div>
        <button className='submit-button transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 duration-300' type="submit" >Login</button>
        </form>
    )
  }
}



const mapStateToProps = (state)=>{
  return{
    isLoggedIn:state.isLoggedIn,
  }
}


const mapDispatchToProps = (dispatch)=>{
  return{
    getLoggeddStatus: (data)=>dispatch(userLoggedIn(data)),
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login)