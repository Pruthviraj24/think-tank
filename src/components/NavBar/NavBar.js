import {NavLink} from "react-router-dom"
import {Component} from "react"
import { connect } from "react-redux"
import {userLoggedIn} from "./../../services/Actions/Actions"
import "./index.css"


class NavBar extends Component{
  
    logOut = ()=>{
        const {getLoggeddStatus} = this.props
        getLoggeddStatus({
            username:"",
            email:"",
            isLoggedIn:true
        })
    }

  render(){
    const {isLoggedIn} = this.props
    return (
        <div className="nav-bar">
                <h1>Think Tank India  <hr/> <small className="moto">Thinks out of the box...</small></h1>
            <div className="nav-items">
                <NavLink to="/" className="navlink">
                    <button>Home</button>
                </NavLink>
                {!isLoggedIn ? <NavLink to="/signin"  className="navlink">
                    <button>Sign in</button>
                </NavLink> : <NavLink to="/"  className="navlink">
                    <button onClick={this.logOut}>Sign Out</button>
                </NavLink>}
                <NavLink to="signup"  className="navlink">
                    <button type="button">Sign up</button>
                </NavLink>
            </div>
        </div>
    )
  }

}

const mapStateToProps = (state)=>{
    return{
        isLoggedIn:state.addUser.isLoggedIn,
    }
  }
  
  const mapDispatchToProps = (dispatch)=>{
    return{
      getLoggeddStatus: ()=>dispatch(userLoggedIn({})),
    }
  }

  
  export default connect(mapStateToProps,mapDispatchToProps)(NavBar)
