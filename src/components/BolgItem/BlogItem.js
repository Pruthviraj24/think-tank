import { Component } from "react"
import { connect } from "react-redux"
import Cookies from "js-cookie"
import {updateFav,followingState} from "./../../services/Actions/Actions"
import {BsFillStarFill,BsStar} from "react-icons/bs"

import "./index.css"
import TimeAgo  from "./../TimeAgo/TimeAgo"

class BlogItem extends Component{
    state = {commentValue:"",commentsArray:[]}

    componentDidMount = ()=>{
        this.getCommentsList()
    }


    getCommentsList = async ()=>{
      const {blog} = this.props
      const {commentValue} = this.state

      const url = `/api/articles/${blog.slug}/comments`

      const jwtToken = Cookies.get('jwt_token')
      const options = {
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
        method:'GET'
      }

      const resposne = await fetch(url, options)

      if(resposne.ok){
        const responseData = await resposne.json()
        const res = responseData.comments
       
        this.setState({
          commentsArray:res
        })
      }else{
        // console.log(resposne);
      }

    }




    commentValueUpdater = (event)=>{
      this.setState({
        commentValue:event.target.value
      })
    }

    postComment = async ()=>{
      const {blog,updateFevHandler} = this.props
     

      const url = !blog.slug ? `/api/articles/${blog.slug}/favorite/` : `/api/articles/${blog.slug}/favorite/`

      const jwtToken = Cookies.get('jwt_token')
      const options = {
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
        method: blog.slug ? 'POST' : "DELETE",
      }

      const resposne = await fetch(url, options)

      if(resposne.ok){
        updateFevHandler(blog)
      }else{
        console.log(resposne);
      }
    }







    updateFevState = async ()=>{
        const {blog,updateFevHandler} = this.props
   
       
        const url = !blog.slug ? `/api/articles/${blog.slug}/favorite/` : `/api/articles/${blog.slug}/favorite/`

        const jwtToken = Cookies.get('jwt_token')
        const options = {
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
          method: blog.slug ? 'POST' : "DELETE",
        }
  
        const resposne = await fetch(url, options)
  
        if(resposne.ok){
          updateFevHandler(blog)
        }else{
          console.log(resposne);
        }

    }

    updateFollowingState = async ()=>{
      const {blog,followingStateHandler,currentUser} = this.props

      const url = !blog.author.following ? `/api/profiles/${currentUser}/follow/` : `/api/profiles/${currentUser}/follow/`

      const jwtToken = Cookies.get('jwt_token')
      const options = {
        headers: {
          Authorization: `Bearer ${jwtToken}`,
        },
        method: !blog.author.following ? 'POST' : "DELETE",
      }

      const resposne = await fetch(url, options)

      if(resposne.ok){
        followingStateHandler(blog)
      }else{
        console.log(resposne);
      }
    }

   render(){
    const {commentValue,commentsArray} = this.state

    const {blog,loggedInState,currentUser} = this.props
        return(
            <li className="blog-list-item">
              <div className="blog-container">
              <div className="blog-sub-container">
                <div className="profile-container">
                    <img className="profile-image" src={blog.author.image} alt={blog.author.username}/>
                    <div>
                    <h2>{blog.author.username}</h2>
                    <p>Created <TimeAgo timestamp = {blog.createdAt}/></p>
                    </div>
         
                    {loggedInState && <button id="follow-button" type="button" onClick={this.updateFollowingState}>{!blog.author.following ? "Follow":"Following"}</button>}
                </div>
                { loggedInState&& <button id="add-to-fav" onClick={this.updateFevState}  type="button">{blog.favorited? "Favorite" :"Add to Favorite" }{ blog.favorited  ? <BsFillStarFill className="fav-start"/>: <BsStar className="non-fev-star"/>}</button> }
                <h3 className="blog-header">{blog.title}</h3>
                <p className="post-containet">{blog.description}</p>
                <hr/>
                <p>{blog.body}</p>
                <ul className="blog-tags-contianer">
                  {
                    blog.tagList.map(eachTag =>{
                      return eachTag && <li key={eachTag}>{`#${eachTag}`}</li>
                    })
                  }
                </ul>
                </div>
                    <div>
                      <img className="main-profile-image" src={blog.author.image} alt={blog.author.username}/>
                    </div>
                </div>
                  {
                    loggedInState &&  
                    <div>
                      {
                        commentsArray.map(eachBlog => 
                        <li>
                          <div>
                            <h4>{eachBlog.author.username}</h4>
                            {/* <p>{eachBlog}</p> */}
                          </div>
                        </li>
                        )
                      }
                      <div>
                              <label htmlFor="comments">Comments</label>
                                <textarea value={commentValue} onChange={this.commentValueUpdater}  id="comments" placeholder="Express your thoughts"></textarea>
                                <button onClick={this.postComment} type="button">Add New Comment</button>
                            </div>
                    </div>
                  }
            </li>
        )
    }
}


const mapStateToProps = (state)=>{
    return{
      articlesData:state.articles,
      loggedInState:state.addUser.isLoggedIn,
      currentUser:state.addUser.username
    }
  }
  
  
  const mapDispatchToProps = (dispatch)=>{
    return{
      updateFevHandler: (data)=>dispatch(updateFav(data)),
      followingStateHandler:(data)=>dispatch(followingState(data))
    }
  }

export default connect(mapStateToProps,mapDispatchToProps)(BlogItem)