import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import NavBar from "./components/NavBar/NavBar";
import SignUp from "./components/SignUp/SignUp";


function App() {
  return (
    <BrowserRouter>
      <NavBar/>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/signin" component={Login}/>
        <Route exact path="/signup" component={SignUp}/>
      </Switch>
    </BrowserRouter>
  )
}

export default App